const express = require('express')
const cors = require('cors')
const routes = require('./routes')
const Mongo = require('./config/mongoose')
const errorHandler = require('./middlewares/errorHandler')
const app = express()
const PORT = process.env.PORT || 3004

const mongo = new Mongo('mongodb://superAdmSurvey:P3rm4t4Surv3y@10.144.95.7:14045/e-responden?authSource=admin&maxPoolSize=3000&readPreference=primary&appname=MongoDB%20Compass&ssl=false')
mongo.connect()
mongo.db.once('open', () => {
  console.log('MongoDb Connected')
})

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(routes)
app.use(errorHandler)

app.listen(PORT, () => {
  console.log('eResponden server is running on port', PORT)
})
