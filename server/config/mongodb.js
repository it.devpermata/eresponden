const { MongoClient, ObjectID } = require('mongodb');

const databaseUrl = 'mongodb://116.193.191.103:14045/e-responden?readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false';
// const databaseUrl = 'mongodb://127.0.0.1:27017/e-responden'

const databaseName = process.env.DATABASE_NAME || 'e-responden';
const client = new MongoClient(databaseUrl, { useUnifiedTopology: true });

client.connect();

const mongo = client.db(databaseName);

module.exports = { mongo, ObjectID };
