const mongoose = require('mongoose')

class MongoDb {
  constructor(uri, useNewUrlParser = true, useUnifiedTopology = false) {
    this._uri = uri
    this._useNewUrlParser = useNewUrlParser
    this._useUnifiedTopology = useUnifiedTopology
  }

  get db () {
    return mongoose.connection
  }

  connect = () => {
    return mongoose.connect(this._uri, { useNewUrlParser: this._useNewUrlParser, useUnifiedTopology: this._useUnifiedTopology })
  }
}


module.exports = MongoDb