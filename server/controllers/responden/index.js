const Responden = require('../../models/Responden')
const Survey = require('../../models/Surveys')
const TkoSurvey = require('../../models/TkoSurveys')
const axios = require('axios')
const User = require('../../models/User')
const moment = require('moment')
const excelDateToJSDate = require('../../helpers/excelDateToJSDate')
// const redis = require('../../config/redis')

module.exports = class RespondenController {
  static Register = async (req, res, next) => {
    try {
      const { Name, Company, Position, Email, PhoneNumber } = req.body

      const Pic = req.logged
      if (!Pic) throw { name: 'Unauthorized' }

      const findPhoneNumber = await Responden.FindOneByPhoneNumber(PhoneNumber)
      if (findPhoneNumber) throw { name: 'RespondenRegistered' }

      const dbResponse = await Responden.InsertOne({ Name, Company, Position, Email, PhoneNumber, Pic, Status: 'Registered' })
      await Survey.InsertOne({
        InputDate: {
          number: new Date().getTime(),
          viewable: moment(new Date()).format('Do MMM YYYY, hh:mm')
        },
        Responden: {
          Nama: Name, Jabatan: Position, Email, TelponHp: PhoneNumber
        }, Perusahaan: { NamaPerusahaan: Company }, RespondenId: dbResponse._id, type: 'CC'
      })
      
      res.status(201).json({
        code: 201,
        message: 'Success',
        data: dbResponse
      })
    } catch(err) {
      next(err);
    }
  }

  static FetchAllSurvey = async (req, res, next) => {
    try {
      // let dbResponse;

      // With Redis (In devs)
      // const rdsSurvey = redis.get('surveys')
      // if (rdsSurvey) {
      //   dbResponse = JSON.parse(rdsSurvey)
      //   res.status(200).json({
      //     code: 200,
      //     message: 'Success',
      //     type: 'Redis',
      //     data: dbResponse
      //   })
      // }

      const [dbResponse, tkoData] = await Promise.all([
        Survey.FindAll(), 
        TkoSurvey.FindAll()
      ])
      console.log(dbResponse)
      let results = []

      for (let i=0; i<dbResponse.length; i++) {
        let obj = dbResponse[i]
        let referrer = await User.FindOneById(obj?.RespondenData[0]?.Pic)
        if (referrer) {
          obj.Referrer = {
            _id: referrer._id,
            Nrk: referrer.Nrk,
            Name: referrer.Name,
            UkDir: referrer.UkDir,
            UkMgr: referrer.UkMgr
          }
        }
        obj.RespondenData = undefined
        results.push(obj)
      }

      results = results.filter(each => each.Referrer)

      // const tkoData = await TkoSurvey.FindAll({ "InputDate": { $exists: true } })

      // redis.set('surveys', JSON.stringify(results), "EX", 600)

      res.status(200).json({
        code: 200,
        message: 'Success',
        data: [...results, ...tkoData],
        type: 'Database'
      })
    } catch (err) {
      next(err)
    }
  }

  static SendSurveyLink = async (req, res, next) => {
    const { id } = req.params
    try {
      // const dbResponse = await Responden.FindOneAndUpdate(id, { Status: 'Link Sent' })
      const dbResponse = await Responden.FindOneById(id)
      const { PhoneNumber, Name } = req.logged
      // const headers = {
      //   Accept: 'application/json',
      //   APIKey: '22833CB97E1ED7459B0222FFB98B51DA'
      // };
      // var data = {
      //   destination: dbResponse.PhoneNumber,
      //   sender: '6283806050705',
      //   message: `Here's your survey link: http://css.permataindonesia.com/${ dbResponse.type === 'TKO' ? 'tko' : 'cc' }/${ id } (Note: Jika link tidak aktif, mohon untuk save nomor ini terlebih dahulu dan coba lagi)`,
      // }
      // const url = 'https://api.wachat-api.com/wachat_api/1.0/message'
    
      // const response = await axios.post(url, data, { headers })
      if (!dbResponse)
        throw { message: 'User Not Found' }
      const headers = {
        Accept: 'application/json',
        Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NDY4ZWJlYTZkMWQ0NGI0M2RmMTkzNzUiLCJmdWxsTmFtZSI6Ik11c2EgQmFnamEiLCJwaG9uZU51bWJlciI6IjYyODEyMTE5MTgxNTAiLCJlbWFpbCI6Im11c2FAcGVybWF0YWluZG9uZXNpYS5jb20iLCJfX3YiOjAsImlhdCI6MTY4NDYwMTg4MX0.uueheJUGGxRd9VWadUUJsxnZVTqXxYBbPNvY1x_uhBQ'
      };
      const data = {
        to: dbResponse.PhoneNumber,
        message: `Hai *${ dbResponse.Name }*,

Terima kasih sebelumnya kami ucapkan atas kerjasama yang sudah terjalin dengan  PT PERMATA INDO SEJAHTERA. 

Mohon untuk meluangkan waktu mengisi survey sebagai bahan evaluasi kami kedepannya dengan klik link berikut:

http://css.permataindonesia.com/cc/${ dbResponse._id }

Semoga kita dapat bekerja sama kembali di masa yang akan datang
  
Terima kasih
*PT Permata Indo Sejahtera - Auto Message*`
      }
      const referrerData = {
        to: PhoneNumber.slice(1),
        message: `Hai *${ Name }*,

Responden yang telah kamu daftarkan dengan nama ${ dbResponse.Name } telah dikirimkan link untuk mengisi survey. 

Mohon untuk meluangkan waktu mengingatkan responden kamu untuk mengisi survey sebagai bahan evaluasi kami kedepannya dengan klik link berikut:

http://css.permataindonesia.com/cc/${ dbResponse._id }

Semoga kita semua dapat bekerja sama kembali di masa yang akan datang
  
Terima kasih
*PT Permata Indo Sejahtera - Auto Message*`
      }
      const url = 'https://api.bilikwa.com/round-robin/send-message'

      const [response, referrerResponse] = await Promise.all([axios.post(url, data, { headers }), axios.post(url, referrerData, { headers })])

      if (referrerResponse.data.error) {
        throw { name: 'Referrer WA api error' }
      }
      if (response.data.error) {
        throw { name: 'WA api error' }
      }
      await Responden.FindOneAndUpdate(id, { Status: 'Link Sent' })

      res.status(200).json({
        code: 200,
        message: 'Success'
      })
    } catch (err) {
      await Responden.FindOneAndUpdate(id, { Status: 'Registered' })
      if (err.response) {
        console.error(err.response.data)
        res.status(500).json({ status: "error" })
      } else if (err.request) {
        console.error(err.request)
        res.status(500).json({ status: "error" })
      } else {
        console.error(err.message);
        res.status(500).json({ status: "error" })
      }
    }
  }

  static SubmitTkoSurvey = async (req, res, next) => {
    try {
      const { Nama, Jabatan, NamaPerusahaan, TelponHp, Kota, StatusTKO, Rekrutmen, PenempatanKerja, PenggunaanBpjs,
      PayrollGaji, PerpanjanganKontrak, ResponPenangananKeluhan, KemudahanKontakStafPIS, AdakahPungutanBiaya, PIS, PerusahaanLain,
      Saran, KerjaSejak, AplikasiMWS,
      AplikasiPendukungPekerjaan,
      AplikasiEmployeeBenefit } = req.body
      const { id } = req.params 
      const dbResponse = await TkoSurvey.FindOneAndUpdate(id, {
        "Responden.Nama": Nama,
        "Responden.Jabatan": Jabatan,
        "Responden.TelponHp": TelponHp,
        "Perusahaan.NamaPerusahaan": NamaPerusahaan,
        "Perusahaan.Kota": Kota,
        "Perusahaan.KerjaSejak": KerjaSejak,
        StatusTKO,
        KualitasLayanan: {
          Rekrutmen,
          PenempatanKerja,
          PenggunaanBpjs,
          PayrollGaji,
          PerpanjanganKontrak
        },
        TeknologiInformasi: {
          AplikasiMWS,
          AplikasiPendukungPekerjaan,
          AplikasiEmployeeBenefit
        },
        Kerjasama: {
          ResponPenangananKeluhan,
          KemudahanKontakStafPIS
        },
        AdakahPungutanBiaya,
        Banding: {
          PIS,
          PerusahaanLain
        },
        Saran
      })
      const { RespondenId } = dbResponse.value
      await Responden.FindOneAndUpdate(RespondenId, { Status: 'Survey Submitted' })

      res.status(200).json({
        code: 200,
        message: 'Success',
        data: dbResponse
      })
      
    } catch (err) {
      next(err)
    }
  }

  static DropCollection = async (req, res, next) => {
    try {
      let allRespondens = await Responden.FindAll()
      for (let i=0; i<allRespondens.length; i++) {
        const obj = allRespondens[i]
        await Responden.FindOneAndDelete(obj._id)
      }
      let allSurvey = await Survey.FindAll()
      for (let i=0; i<allSurvey.length; i++) {
        const obj = allSurvey[i]
        await Survey.FindOneAndDelete(obj._id)
      }

      res.status(200).json({
        code: 200,
        message: 'Success'
      })
    } catch (err) {
      next(err)
    }
  }

  static SubmitCcSurvey = async (req, res, next) => {
    try {
      const { Nama, Jabatan, NamaPerusahaan, Email, TelponHp, Alamat, PosisiPisDiPerusahaan, RuangLingkupJasa, KualitasLayanan,
      KualitasTKO, KetepatanWaktu, PemeliharaanHubKlien, PemeliharaanHubTKO, ResponPenangananKeluhan, KemudahanKontakStafPIS,
      PIS, PerusahaanLain, Saran, TelponPerusahaan, UnitKerja, Ekstensi, AreaOfInvolvement, AplikasiMWS,
      AplikasiPendukungPekerjaan,
      AplikasiEmployeeBenefit } = req.body
      const { id } = req.params
      const dbResponse = await Survey.FindOneAndUpdate(id, {
        PosisiPisDiPerusahaan,
        RuangLingkupJasa,
        AreaOfInvolvement, 
        Responden: {
          Nama, Jabatan, Email, TelponHp, UnitKerja, Ekstensi
        },
        Perusahaan: {
          NamaPerusahaan, Alamat, TelponPerusahaan
        },
        KualitasLayanan: {
          KualitasLayanan,
          KualitasTKO,
          KetepatanWaktu,
          PemeliharaanHubKlien,
          PemeliharaanHubTKO
        },
        TeknologiInformasi: {
          AplikasiMWS,
          AplikasiPendukungPekerjaan,
          AplikasiEmployeeBenefit
        },
        Kerjasama: {
          ResponPenangananKeluhan,
          KemudahanKontakStafPIS
        },
        Banding: {
          PIS,
          PerusahaanLain
        },
        Saran
      })
      const { RespondenId } = dbResponse.value
      await Responden.FindOneAndUpdate(RespondenId, { Status: 'Survey Submitted' })

      res.status(200).json({
        code: 200,
        message: 'Success',
        data: dbResponse
      })
    } catch (err) {
      next(err)
    }
  }

  static GetSurvey = async (req, res, next) => {
    try {
      const { phonenumber } = req.params
      const { isTko } = req.query
      let dbResponse

      if (isTko) dbResponse = await TkoSurvey.FindOneByRespondenPhoneNumber(phonenumber)
      else dbResponse = await Survey.FindOneByRespondenPhoneNumber(phonenumber)
      if (!isTko) {
        console.log(dbResponse, 'cc')
      }
      if (!dbResponse) throw { name: 'SurveyNotFound' }
      if (dbResponse.err) throw { name: 'SurveyNotFound' }

      res.status(200).json({
        code: 200,
        message: 'Success',
        data: dbResponse
      })
    } catch (err) {
      next(err)
    }
  }

  static GetTkoSurveyById = async (req, res, next) => {
    try {
      const { id } = req.params

      const dbResponse = await TkoSurvey.FindOneById(id)

      res.status(200).json({
        code: 200,
        message: 'Success',
        data: dbResponse
      })
    } catch (error) {
      next(err)
    }
  }

  static GetSurveyById = async (req, res, next) => {
    try {
      const { id } = req.params

      const dbResponse = await Survey.FindOneByRespondenId(id)

      res.status(200).json({
        code: 200,
        message: 'Success',
        data: dbResponse
      })
    } catch (error) {
      next(err)
    }
  }

  static Get = async (req, res, next) => {
    try {
      const { _id } = req.logged

      const user = await User.FindOneById(_id)
      const dbResponse = await Responden.AggregateByRegion(user.Region)
      res.status(200).json({
        code: 200,
        message: 'Success',
        data: dbResponse
      })
    } catch(err) {
      next(err);
    }
  }

  static Delete = async (req, res, next) => {
    try {
      const { id } = req.params
      const respondenData = await Responden.FindOneById(id)
      await Survey.FindOneByRespondenPhoneNumberAndDelete(respondenData.PhoneNumber)
      const dbResponse = await Responden.FindOneAndDelete(id)

      res.status(200).json({
        code: 200,
        message: 'Success',
        data: dbResponse
      })
    } catch(err) {
      next(err)
    }
  }

  static FindOneById = async (req, res, next) => {
    try {
      const { id } = req.params
      const dbResponse = await Responden.FindOneById(id)

      if (!dbResponse) throw { name: 'RespondenNotFound' }

      res.status(200).json(dbResponse)
    } catch(err) {
      next(err)
    }
  }

  static GetCompanies = async (req, res, next) => {
    try {
      const result = await axios({
        url: 'http://ios.permataindonesia.com/api/get-client-active',
        method: 'GET'
      })

      let companies = result.data

      companies = companies.map(each => ({ name: each.name, iosId: each.iosId, code: each.code }))

      res.status(200).json({
        code: 200,
        response: {
          companies
        }
      })
    } catch (error) {
      next(error)
    }
  }

  static ClearNoResponden = async (req, res, next) => {
    try {
      const dbResponse = await Survey.ClearNoResponden()
      
      res.status(200).json({
        code: 200,
        message: 'Success',
        data: dbResponse
      })
    } catch (err) {
      next(err)
    }
  }

  static AcceptToResign = async (req, res, next) => {
    try {
      moment.locale('id')
      const { nrk, nama, client, phone, jenis_kontrak, kontrak_start, area } = req.body
      const dbResponse = await TkoSurvey.InsertOne({
        "Responden": {
          "Nama": nama,
          "Nrk": nrk ? +nrk : null,
          "TelponHp": phone ? phone.slice(1) : null
        },
        "Perusahaan": {
          "NamaPerusahaan": client,
          "Kota": area,
          "KerjaSejak": kontrak_start ? moment(new Date(kontrak_start)).format("l") : null
        },
        "StatusTKO": jenis_kontrak,
        "Origin": req.body,
        "InputDate": {
          number: new Date().getTime(),
          viewable: moment(new Date()).format('Do MMM YYYY, hh:mm')
        }
      })
      // moment(new Date(excelDateToJSDate(el.kontrak_start)).getTime() - 1000*60*60*24).format("l")
      const headers = {
        Accept: 'application/json',
        Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NDY4ZWJlYTZkMWQ0NGI0M2RmMTkzNzUiLCJmdWxsTmFtZSI6Ik11c2EgQmFnamEiLCJwaG9uZU51bWJlciI6IjYyODEyMTE5MTgxNTAiLCJlbWFpbCI6Im11c2FAcGVybWF0YWluZG9uZXNpYS5jb20iLCJfX3YiOjAsImlhdCI6MTY4NDYwMTg4MX0.uueheJUGGxRd9VWadUUJsxnZVTqXxYBbPNvY1x_uhBQ'
      };

      // *Hai Nangsky*,
 
      // Terima kasih sebelumnya kami ucapkan sudah menjadi bagian dari *PT PERMATA INDO SEJAHTERA*. 

      // Yuk bantu kami dengan meluangkan waktu kamu sejenak untuk kasih review sebagai bahan evaluasi kami kedepannya. 
      // Jangan lupa klik link berikut ya :

      // https://kerja365.id/

      // Terima kasih
      // *PT Permata Indo Sejahtera - Auto Message*
      const data = {
        to: phone.slice(1),
        message: `Hai *${ nama }*,

Terima kasih sebelumnya kami ucapkan sudah menjadi bagian dari *PT PERMATA INDO SEJAHTERA*. 
  
Yuk bantu kami dengan meluangkan waktu kamu sejenak untuk kasih review sebagai bahan evaluasi kami kedepannya. 
Jangan lupa klik link berikut ya :
  
http://css.permataindonesia.com/tko/${ dbResponse.insertedId }
  
Terima kasih
*PT Permata Indo Sejahtera - Auto Message*`,
      }
      const url = 'https://api.bilikwa.com/round-robin/send-message'

      const response = await axios.post(url, data, { headers })

      res.status(201).json({
        code: 201,
        message: 'Success',
        dbResponse,
        waResponse: response.data
      })
    } catch (error) {
      console.log(error)
      if (error.response) {
        console.error(error.response.data)
      } else if (error.request) {
        console.error(error.request)
      } else {
        console.error(error.message);
      }
      next(error)
    }
  }

  static base64accept = (req, res, next) => {
    try {
      console.log(req.body)
      const { base64image } = req.body

      res.status(200).json({
        code: 200,
        result: base64image
      })
    } catch (error) {
      next (error)
    }
  }

  static playground = async (req, res, next) => {
    try {
      // const excelReferrerImportee = require('../../helpers/xlsxReader')

      // const source = excelReferrerImportee()
      
      // for (let key of source) {
      //   const findOne = await User.FindOneByNrk(key.Nrk)

      //   if (findOne) {
      //     await User.UpdateOne({ Nrk: key.Nrk }, { ...key })
      //   } else {
      //     await User.Insert({ ...key })
      //   }
      // }

      const resignedUsers = require('../../helpers/xlsxReader')
      const parsedIDs = resignedUsers().map(each => ({ Nrk: "" + each.Nrk }))
      console.log(parsedIDs)

      await User.DeleteMany({ $or: parsedIDs })
      res.status(200).json({
        message: "Ok",
        response: {
          source: parsedIDs
        }
      })
    } catch (error) {
      next (error)
    }
  }
}