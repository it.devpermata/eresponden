const User = require('../../models/User')
const Bcrypt = require('../../helpers/bcrypt')
const Jwt = require('../../helpers/jwt')
const Responden = require('../../models/Responden')

module.exports = class UserController {
  static Login = async (req, res, next) => {
    try {
      const { Nrk, Password } = req.body

      if (!Nrk) throw { name: 'NoNrk' }
      const userData = await User.FindOneByNrk(Nrk)
      if (!userData) throw { name: 'NrkUnregistered' }
      if (!Password || !Bcrypt.compare(Password, userData.Password)) throw { name: 'WrongPassword' }

      const accessToken = Jwt.sign({
        _id: userData._id,
        Nrk: userData.Nrk,
        Name: userData.Name,
        PhoneNumber: userData.PhoneNumber,
        Position: userData.Position
      })

      res.status(200).json({
        code: 200,
        message: 'Success',
        access_token: accessToken,
        _id: userData._id
      })
    } catch(err) {
      next(err)
    }
  }

  static GetProfile = (req, res, next) => {
    try {
      const dbResponse = req.logged

      res.status(200).json({
        code: 200,
        message: 'Success',
        data: dbResponse
      })
    } catch (err) {
      next(err)
    }
  }

  static GetAll = async (req, res, next) => {
    try {
      let results = []

      const dbResponse = await User.FindAll()

      for (let i=0; i<dbResponse.length; i++) {
        let obj = dbResponse[i]
        let respondens = await Responden.FindByPic(obj._id)
        obj.UkSpv = respondens.length
        await results.push(obj)
      }
      
      res.status(200).json({
        code: 200,
        message: 'Success',
        data: results
      })
    } catch (err) {
      next(err)
    }
  }

  static Seed = async (req, res, next) => {
    try {
      const dbResponse = await User.FindAll()
      let counter = 0

      const parsed = require('../../assets/user_referrer.json')
      // for (let key of parsed) {
      //   for(let id of dbResponse) {
      //     if (id.Name === key.Name) {
      //       await User.UpdateOne({ Name: key.Name }, { ...key })
      //     }
      //   }
      // }

      for (let key of parsed) {
        const find = dbResponse.find(each => key.Name === each.Name)
        if (!find) {
          counter++
          await User.Insert(key)
        }
      }

      res.send(dbResponse)
    } catch (error) {
      next(error)
    }
  }
}