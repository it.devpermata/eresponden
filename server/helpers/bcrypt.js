const bcrypt = require('bcryptjs')

module.exports = class Bcrypt {
  static hash = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10))
  }

  static compare = (password, hashed) => {
    return bcrypt.compareSync(password, hashed)
  }
}