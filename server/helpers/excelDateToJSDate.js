module.exports = (excelDate) => {
    var date = new Date(Math.round((excelDate - (25567 + 1)) * 86400 * 1000));
    var converted_date = date.toISOString().split('T')[0];
    return converted_date;
  }