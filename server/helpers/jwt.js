const jwt = require('jsonwebtoken')

module.exports = class Jwt {
  static sign = (payload) => {
    return jwt.sign(payload, 'eRespSec')
  }

  static verify = (token) => {
    return jwt.verify(token, 'eRespSec')
  }
}