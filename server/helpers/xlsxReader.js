const XLSX = require('xlsx')
const workbook = XLSX.readFile('./assets/cleansing-referrer.xlsx')
const fs = require('fs')
const Bcrypt = require('./bcrypt')
const moment = require('moment')
const User = require('../models/User')

const xlsxReader = (book) => {
  let worksheets = {}

  for (const sheetName of book.SheetNames) {

    worksheets[sheetName] = XLSX.utils.sheet_to_json(book.Sheets[sheetName])
  }
  return worksheets
}

// console.log(xlsxReader(workbook).referrral.map(each => ({
//   UkDir: each.UK,
//   UkMgr: each.UK,
//   UkSpv: each.UK,
//   Location: each.area,
//   Nrk: '' + each.nrk,
//   Name: each.nama,
//   Area: each.area,
//   Region: each.regional,
//   PhoneNumber: `${each.tlp}`.startsWith('62') ? '+' + `${each.tlp}` : '+62' + `${each.tlp}`,
//   Position: each.posisi,
//   Password: Bcrypt.hash(`${each.tlp}`.slice(`${each.tlp}`.length - 4, `${each.tlp}`.length))
// })))

// 2023

// Cleansing latest exit referrer
const cleanseResignReferrer = () => {
  return xlsxReader(workbook).Sheet3
}

console.log(cleanseResignReferrer())

// import XLSX latest referrer
// const importXlsxReferrerToday = () => {
//   return xlsxReader(workbook).referrral.map(each => ({
//     UkDir: each.UK,
//     UkMgr: each.UK,
//     UkSpv: each.UK,
//     Location: each.area,
//     Nrk: '' + each.nrk,
//     Name: each.nama,
//     Area: each.area,
//     Region: each.regional,
//     PhoneNumber: `${each.tlp}`.startsWith('62') ? '+' + `${each.tlp}` : '+62' + `${each.tlp}`,
//     Position: each.posisi,
//     Password: Bcrypt.hash(`${each.tlp}`.slice(`${each.tlp}`.length - 4, `${each.tlp}`.length))
//   }))
// }




module.exports = cleanseResignReferrer

// fs.writeFileSync('../assets/hasil_tko_survey.json', JSON.stringify(xlsxReader(workbook).data.filter(each => each["Kerja Sejak"] === 'Invalid date'), null, 2))


// ========================= USER DATA JSON ================================
// let results = []

// xlsxReader(workbook)["Dakar per 0721"].forEach(el => {
//   let find = xlsxReader(workbook)["Dakar HP per 2109"].find(elm => +elm.NRK === +el.NIK)
//   if (find) results.push({ ...el, ...find })
// })

// results = results.filter(el => el.REFERRER === 'Y').map(el => {
//   return {
//     Business: el.BUSINESS,
//     UkDir: el["UK SELEVEL DIR"],
//     UkMgr: el["UK SELEVEL MGR"],
//     UkSpv: el["UK SELEVEL SPV"],
//     Location: el.LOKASI,
//     Nrk: el.NIK,
//     Name: el.Nama,
//     PlaceOfBirth: el["Tempat lahir"],
//     DateOfBirth: el["Tanggal lahir"],
//     Gender: el.Kelamin,
//     Religion: el.Agama,
//     BloodGroup: el["Gol Darah"],
//     MaritalStatus: el["Status Kawin"],
//     Area: el.Area,
//     Region: el.Regional,
//     PhoneNumber: el.Telp,
//     Position: el.Jabatan,
//     Password: Bcrypt.hash(el.Telp.slice(el.Telp.length - 4, el.Telp.length))
//   }
// })
// results = [...results, {
//   Nrk: '0202100999',
//   Name: 'MASTER',
//   Password: Bcrypt.hash('permata123')
// }]
// fs.writeFileSync('../assets/user_data.json', JSON.stringify(results, null, 2))


// ========================= COMPANY DATA JSON ================================
// const companyBook = XLSX.readFile('../assets/company_data.xlsx')

// console.log(xlsxReader(companyBook).Sheet1.filter(el => el.client_status === 'Active'))

// const resultBook = xlsxReader(companyBook).Sheet1.filter(el => el.client_status === 'Active').map((el, ind) => {
//   return { 
//     id: ind,
//     company: el.client_name
//   }
// })

// ========================= TKO DATA JSON ====================================
// const tkoBook = XLSX.readFile('../assets/tko_data.xlsx')

// console.log(xlsxReader(tkoBook).Sheet1)


// const excelDateToJSDate = (excelDate) => {
//   var date = new Date(Math.round((excelDate - (25567 + 1)) * 86400 * 1000));
//   var converted_date = date.toISOString().split('T')[0];
//   return converted_date;
// }

// const tkoResults = xlsxReader(tkoBook).Sheet1.map(el => {
//   let telponHp = ''
//   for (let i=1; i<el.phone.length; i++) telponHp+=el.phone[i]
//   return {
//     Responden: {
//       Nama: el.nama,
//       Nrk: el.nrk,
//       TelponHp: telponHp
//     },
//     Perusahaan: {
//       NamaPerusahaan: el.client,
//       Kota: el.area,
//       KerjaSejak: el.kontrak_start ? moment(new Date(excelDateToJSDate(el.kontrak_start)).getTime() - 1000*60*60*24).format("l") : null
//     },
//     StatusTKO: el.jenis_kontrak.includes('PKWT') ? 'PKWT' : 'PKM',
//     Origin: {
//       ...el
//     }
//   }
// })

// console.log(tkoResults)
// fs.writeFileSync('../assets/tko_data.json', JSON.stringify(tkoResults, null, 2))

// ========================= TKO DATA MASTER JSON ====================================

// const book = XLSX.readFile('../assets/tko_data_master.xlsx')
// const tkoBook = XLSX.readFile('../assets/tko_data.xlsx')

// // console.log(xlsxReader(book)["data master 2"])


// // const excelDateToJSDate = (excelDate) => {
// //   var date = new Date(Math.round((excelDate - (25567 + 1)) * 86400 * 1000));
// //   var converted_date = date.toISOString().split('T')[0];
// //   return converted_date;
// // }

// const results = xlsxReader(book)["data master 2"].map(el => {
//   let region = el.REGION
//   let phoneNumber = el["Telpon Hp Responden"].slice(2)

//   let regional = xlsxReader(tkoBook).Sheet1.find(elm => elm.nama === el['Nama Responden'] || `${elm.phone}`.includes(phoneNumber))?.regional

//   return { ...el, REGIONAL: regional, REGION: el.REGION ? el.REGION : regional }
  
// })
// console.log(xlsxReader(tkoBook).Sheet1)
// console.log(results)

// // console.log(results)
// fs.writeFileSync('../assets/tko_master_data.json', JSON.stringify(results, null, 2))

// // module.exports = results

// ========================= REFERRER UPDATE JSON ====================================

// const referrer = XLSX.readFile('../assets/referrer_2023.xlsx')


// function resultParser () {
//   const source = xlsxReader(referrer)['update 2023']
//   return source.filter(each => each.PhoneNumber && each.KETERANGAN !== "ALM" && each.KETERANGAN !== "NOT ELIGIBLE" && each.KETERANGAN !== "RESIGN").map(each => {
//     let startIdx = 0

//     if (("" + each.PhoneNumber)[0] === "6") {
//       startIdx = 2
//     }
//     else if (("" + each.PhoneNumber)[0] === "+") {
//       startIdx = 3
//     }
//     else if (("" + each.PhoneNumber)[0] === "0") {
//       startIdx = 1
//     }

//     return ({
//       ...each,
//       Nrk: "" + each.Nrk,
//       PhoneNumber: "+62" + (("" + each.PhoneNumber).slice(startIdx)),
//       Password: Bcrypt.hash(("" + each.PhoneNumber).slice(("" + each.PhoneNumber).length - 4, ("" + each.PhoneNumber).length))
//     })
//   })
// }

// fs.writeFileSync('../assets/user_referrer.json', JSON.stringify(resultParser(), null, 2))

// console.log(resultParser().length)

// console.log(Bcrypt.hash("8594"))
