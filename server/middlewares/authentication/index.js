const User = require('../../models/User')
const Jwt = require('../../helpers/jwt')

const authenticate = async (req, res, next) => {
  try {
    const { access_token } = req.headers
    if (!access_token) throw { name: 'Unauthorized' }
    const user = Jwt.verify(access_token)
    
    const dbUser = await User.FindOneById(user._id)
    if (!dbUser) throw { name: 'Unauthorized' }

    req.logged = dbUser

    next()
  } catch(err) {
    next(err)
  }
}

const authorize = async (req, res, next) => {
  try {

  } catch(err) {
    next(err)
  }
}

module.exports = authenticate, { authorize }