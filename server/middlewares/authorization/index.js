const Responden = require('../../models/Responden')
const mongoose = require('mongoose');
const { Schema, Types: { ObjectId } } = mongoose;

module.exports = class Authorize {
  static Responden = async (req, res, next) => {
    try {
      const { _id } = req.logged
      const { id } = req.params
      const respondenData = await Responden.FindOneById(id)

      if (!respondenData) throw { name: 'RespondenNotFound' }
      if (JSON.stringify(respondenData.Pic) !== JSON.stringify(_id)) throw { name: 'IllegalEdit' }

      next()
    } catch (err) {
      next(err)
    }
  }
}