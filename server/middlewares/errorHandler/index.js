const errorHandler = (err, req, res, next) => {
  let status;
  let message;

  console.log(err)

  switch (err.name) {
    case 'NoNrk':
      status = 401;
      message = 'Please fill the Nrk field';
      break;
    case 'NrkUnregistered':
      status = 401;
      message = 'Nrk unregistered';
      break;
    case 'JsonWebTokenError':
      status = 401;
      message = 'Unauthorized';
      break;
    case 'WrongPassword':
      status = 401;
      message = 'Wrong password';
      break;
    case 'Unauthorized':
      status = 401;
      message = 'Unauthorized';
      break;
    case 'RespondenRegistered':
      status = 400;
      message = 'Responden has already been Registered';
      break;
    case 'RespondenNotFound':
      status = 404;
      message = `Responden can't be found`;
      break;
    case 'SurveyNotFound':
      status = 404;
      message = 'Survey not found';
      break;
    case 'Referrer WA api error':
      status = 403;
      message = 'Referrer WA api error';
      break;
    case 'WA api error':
      status = 403;
      message = 'WA api error';
    default:
      status = 500;
      message = 'Internal Server Error';
  }

  res.status(status).json({
    code: status,
    message
  })
}

module.exports = errorHandler;