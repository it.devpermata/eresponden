const mongoose = require('mongoose');
const { Schema, Types: { ObjectId } } = mongoose;

const respondenSchema = new Schema({
  Name: { type: String, required: true },
  Company: { type: String, required: true },
  Position:  { type: String, required: true },
  Email: { type: String, required: true, unique: true },
  PhoneNumber: { type: String, required: true, unique: true },
  Pic: { type: ObjectId, required: true },
  Status: { type: String, required: true }
}, { collection: 'respondens' })

const Db = mongoose.model('respondens', respondenSchema);

module.exports = class Responden {
  static FindByPic = async (pic) => {
    try {
      let results = []
      const cursor = await Db.find({ Pic: ObjectId(pic) })
      await cursor.forEach(doc => results.push(doc))
      return results
    } catch(err) {
      return { err }
    }
  }

  static FindOneById = async (id) => {
    try {
      return await Db.findOne({ _id: ObjectId(id) })
    } catch (err) {
      return { err }
    }
  }

  static FindOneByEmail = async (email) => {
    try {
      return await Db.findOne({ Email: email })
    } catch(err) {
      return { err }
    }
  }

  static FindOneByPhoneNumber = async (phoneNumber) => {
    try {
      return await Db.findOne({ PhoneNumber: phoneNumber })
    } catch(err) {
      return { err }
    }
  }

  static InsertOne = async (responden) => {
    try {
      return await Db.create({ ...responden })
    } catch(err) {
      return { err }
    }
  }

  static FindOneAndUpdate = async (id, update) => {
    try {
      return await Db.findOneAndUpdate({ _id: ObjectId(id) }, update)
    } catch(err) {
      return { err }
    }
  }

  static FindOneAndDelete = async (id) => {
    try {
      return await Db.deleteOne({ _id: ObjectId(id) })
    } catch (err) {
      return { err }
    }
  }

  static FindAll = async () => {
    try {
      let results = []
      const cursor = await Db.find()
      await cursor.forEach(doc => results.push(doc))
      return results
    } catch (err) {
      return { err }
    }
  }

  static AggregateByRegion = async (region) => {
    try {
      const respondens = await Db.aggregate([{
        $lookup: {
          from: 'users',
          foreignField: '_id',
          localField: 'Pic',
          as: 'user'
        }
      }, {
        $unwind: '$user'
      }, {
        $match: {
          "user.Region": region
        }
      }])
      return respondens
    } catch (error) {
      console.log(error, 'error')
      return { error }
    }
  }
}