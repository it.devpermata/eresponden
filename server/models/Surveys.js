const { ObjectId } = require('bson')
const { mongo, ObjectID } = require('../config/mongodb')
const Db = mongo.collection('surveys')

module.exports = class Survey {
  static InsertOne = async (surveyData) => {
    try {
      return await Db.insertOne(surveyData)
    } catch(err) {
      return { err }
    }
  }

  static FindAll = async () => {
    try {
      let results = []
      const cursor = await Db.aggregate([{
        $lookup: {
          from: 'respondens',
          localField: 'Responden.TelponHp',
          foreignField: 'PhoneNumber',
          as: 'RespondenData'
        }
      }])
      await cursor.forEach(doc => results.push(doc))
      return results.filter(each => each)
    } catch (err) {
      return { err }
    }
  }

  static FindOneByRespondenId = async (respondenId) => {
    try {
      return await Db.findOne({ RespondenId: ObjectId(respondenId) })
    } catch (err) {
      return { err }
    }
  }

  static FindOneByRespondenPhoneNumber = async (PhoneNumber) => {
    try {
      return await Db.findOne({ "Responden.TelponHp": PhoneNumber })
    } catch (err) {
      return { err }
    }
  }

  static FindOneByRespondenPhoneNumberAndDelete = async (PhoneNumber) => {
    try {
      return await Db.findOneAndDelete({ "Responden.TelponHp": PhoneNumber })
    } catch (err) {
      return { err } 
    }
  }

  static FindOneAndUpdate = async (id, update) => {
    try {
      return await Db.findOneAndUpdate({ _id: ObjectId(id) }, { $set: update })
    } catch (err) {
      return { err }
    }
  }

  static ClearNoResponden = async () => {
    try {
      const results = []
      const cursor = await Db.find({ RespondenId: { $not: { $gt: null } } })
      await cursor.forEach((doc) => results.push(doc))
      return results
    } catch (err) {
      return { err }
    }
  }

  static FindOneAndDelete = async (id) => {
    try {
      return await Db.findOneAndDelete({ _id: ObjectId(id) })
    } catch (err) {
      return { err }
    }
  }
}
