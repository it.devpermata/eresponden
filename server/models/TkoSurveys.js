const { ObjectId } = require('bson')
const { mongo, ObjectID } = require('../config/mongodb')
const Db = mongo.collection('tko-surveys')

module.exports = class Survey {
  static InsertOne = async (surveyData) => {
    try {
      return await Db.insertOne(surveyData)
    } catch(err) {
      return { err }
    }
  }

  static FindAll = async () => {
    try {
      let results = []
      const cursor = await Db.find({ InputDate: {
        $exists: true
      } })
      await cursor.forEach(doc => results.push({ ...doc, type: 'TKO' }))
      return results
    } catch (err) {
      return { err }
    }
  }

  static FindOneById = async (Id) => {
    try {
      return await Db.findOne({ _id: ObjectId(Id) })
    } catch (err) {
      return { err }
    }
  }

  static FindOneByRespondenPhoneNumber = async (PhoneNumber) => {
    try {
      return await Db.findOne({ "Responden.TelponHp": PhoneNumber })
    } catch (err) {
      return { err }
    }
  }

  static FindOneByRespondenPhoneNumberAndDelete = async (PhoneNumber) => {
    try {
      return await Db.findOneAndDelete({ "Responden.TelponHp": PhoneNumber })
    } catch (err) {
      return { err } 
    }
  }

  static FindOneAndUpdate = async (id, update) => {
    try {
      return await Db.findOneAndUpdate({ _id: ObjectId(id) }, { $set: update })
    } catch (err) {
      return { err }
    }
  }

  static FindOneAndDelete = async (id) => {
    try {
      return await Db.findOneAndDelete({ _id: ObjectId(id) })
    } catch (err) {
      return { err }
    }
  }

  static FindManyAndDelete = async (filter) => {
    try {
      return await Db.deleteMany(filter)
    } catch (error) {
      return { err }
    }
  }
}
