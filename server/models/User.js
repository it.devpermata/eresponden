const mongoose = require('mongoose');
const { Schema, Types: { ObjectId } } = mongoose;

const userSchema = new Schema({
  Name: { type: String, required: true },
  Business: { type: String },
  UkDir: { type: String },
  UkMgr: { type: String },
  UkSpv: { type: String },
  Location: { type: String },
  Nrk: { type: String },
  Name: { type: String },
  PlaceOfBirth: { type: String },
  DateOfBirth: { type: String },
  Gender: { type: String },
  Religion: { type: String },
  BloodGroup: { type: String },
  MaritalStatus: { type: String },
  Area: { type: String },
  Region: { type: String },
  PhoneNumber: { type: String },
  Position: { type: String },
  Password: { type: String }
}, { collection: 'users' })

// Business: el.BUSINESS,
// UkDir: el["UK SELEVEL DIR"],
// UkMgr: el["UK SELEVEL MGR"],
// UkSpv: el["UK SELEVEL SPV"],
// Location: el.LOKASI,
// Nrk: el.NIK,
// Name: el.Nama,
// PlaceOfBirth: el["Tempat lahir"],
// DateOfBirth: el["Tanggal lahir"],
// Gender: el.Kelamin,
// Religion: el.Agama,
// BloodGroup: el["Gol Darah"],
// MaritalStatus: el["Status Kawin"],
// Area: el.Area,
// Region: el.Regional,
// PhoneNumber: el.Telp,
// Position: el.Jabatan

const Db = mongoose.model('users', userSchema);

module.exports = class User {
  static Insert = async (userData) => {
    try {
      return await Db.create(userData)
    } catch(err) {
      return { err }
    }
  }

  static DeleteMany = async (filter) => {
    try {
      return await Db.deleteMany(filter)
    } catch (err) {
      return { err }
    }
  }

  static FindOneByNrk = async (userNrk) => {
    try {
      return await Db.findOne({ $or: [{ Nrk: userNrk }, { Nrk: '0' + userNrk }] })
    } catch(err) {
      return { err }
    }
  }

  static FindOneById = async (userId) => {
    try {
      return await Db.findOne({ _id: new ObjectId(userId) })
    } catch(err) {
      return { err }
    }
  }

  static FindAll = async () => {
    try {
      const results = []
      const cursor = await Db.find()
      await cursor.forEach(doc => results.push(doc));
      return results
    } catch (err) {
      return { err }
    }
  }

  static UpdateOne = async (filter, update) => {
    try {
      return Db.updateOne(filter, update)
    } catch (error) {
      return { err }
    }
  }
}
