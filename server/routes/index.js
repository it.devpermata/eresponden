const router = require('express').Router()
const Responden = require('../controllers/responden')
const User = require('../controllers/user')
const authenticate = require('../middlewares/authentication')
const Authorization = require('../middlewares/authorization')
const axios = require('axios')

router.get('/playground', Responden.playground)
router.post('/login', User.Login)
router.get('/', (req, res) => {
  res.status(200).json('Server Running')
})
router.post('/wa/send', async (req, res) => {
  try {
    const headers = {
      Accept: 'application/json',
      code: 'kYMjuKbUkwxnKXMbeWMMvfVeuxVFThfQ7SYceLHa7RnGrLuwj68DXEv32fyx9qc7TMUX9tJ69Hpb5txdpfGWKMQCDdKDuymca6L996QUDPVj7hn69FTJ4zWypx2tUcvF'
    };
    var data = {
      phone: req.body.phone,
      message: req.body.message,
    }
    const url = 'https://dwh.permataindonesia.com/api/wa/send'
    const response = await axios.post(url, data, { headers })
    res.status(200).json({
      code: 200,
      message: response.data.status
    })
  } catch (error) {
    if (error.response) {
      console.error(error.response.data)
      res.status(500).json({ status: "error" })
    } else if (error.request) {
      console.error(error.request)
      res.status(500).json({ status: "error" })
    } else {
      console.error(error.message);
      res.status(500).json({ status: "error" })
    }
  }
})
router.post('/base64', Responden.base64accept)
router.get('/survey/tko/:id', Responden.GetTkoSurveyById)
router.get('/survey/cc/:id', Responden.GetSurveyById)
router.get('/survey/:phonenumber', Responden.GetSurvey)
router.post('/tko/:id', Responden.SubmitTkoSurvey)
router.post('/cc/:id', Responden.SubmitCcSurvey)
router.delete('/survey', Responden.ClearNoResponden)
router.delete('/drop', Responden.DropCollection)
router.post('/tko-resign', Responden.AcceptToResign)
router.get('/seed-user', User.Seed)
router.get('/company', Responden.GetCompanies)
router.delete('/responden/:id', Responden.Delete)
router.get('/responden/:id', Responden.FindOneById)
router.use(authenticate)
router.get('/profile', User.GetProfile)
router.get('/referrer', User.GetAll)
router.get('/responden', Responden.Get)
router.post('/responden', Responden.Register)
router.get('/survey/', Responden.FetchAllSurvey)
router.post('/survey/link/:id', Responden.SendSurveyLink)

module.exports = router