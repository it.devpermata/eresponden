import axios from '../axios'

const getSurveyById = (id, type) => {
  return axios({
    url: `/survey/${ type }/${ id }`,
    method: 'GET'
  })
}

export default getSurveyById