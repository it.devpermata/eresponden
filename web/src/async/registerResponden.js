import axios from '../axios'

const registerResponden = (respondenData, access_token) => new Promise((resolve, reject) => {
  axios({
    method: 'POST',
    url: '/responden',
    data: respondenData,
    headers: {
      access_token
    }
  }).then(({ data }) => {
    resolve(data)
  }).catch(err => {
    reject(err)
  })
})

export default registerResponden