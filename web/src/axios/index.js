import axios from 'axios'
import { URL, LOCAL_URL } from '../server_url.json'

export default axios.create({
  baseURL: `${ URL }/`
})
