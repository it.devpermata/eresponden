export { default as LoginPage } from './LoginPage';
export { default as Dashboard } from './Dashboard';
export { default as Quizionaire } from './Quizionaire';
export { default as NotFoundPage } from './NotFoundPage';
export { default as LoadingPage } from './LoadingPage';
export { default as SubmittedPage } from './SubmittedPage';
export { default as MasterDashboard } from './Dashboard/Master';
export { default as ReferrerPage } from './ReferrerPage';