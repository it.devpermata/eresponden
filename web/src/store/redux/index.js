import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { responden, survey, referrer, profile } from './reducers'

export default createStore(combineReducers({ 
  responden, survey, profile, referrer
}), compose(applyMiddleware(thunk)))
